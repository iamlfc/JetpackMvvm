package me.hgj.jetpackmvvm.demo.app.util.network;

/**
*  网络改变观察者，观察网络改变后回调的方法
*  @author lfc-LFC
*  created at 2021/6/29 17:42
*/
public interface NetChangeObserver {

    /**
     * 网络连接回调 type为网络类型
     */
     void onNetConnected(NetType type);

    /**
     * 没有网络
     */
     void onNetDisConnect();
}
