package me.hgj.jetpackmvvm.demo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.demo.R
import me.hgj.jetpackmvvm.demo.app.base.BaseAct
import me.hgj.jetpackmvvm.demo.databinding.ActivityTestMyBinding

class TestMyA : BaseAct<BaseViewModel, ActivityTestMyBinding>() {
    override val bindingInflater: (LayoutInflater) -> ActivityTestMyBinding
        get() = ActivityTestMyBinding::inflate

    override fun initView(savedInstanceState: Bundle?) {
        setToolbarShow(false)

        binding.layInfo1.tvTitle.setText("6666")
        binding.tvTitle.setText("鸡你太美!!")
    }

    fun DoClick(v: View) {
        when (v?.id) {
            R.id.tv_title -> {
                showLoading("你是电你时光")
            }
            R.id.iv_show -> {

            }
            else -> {
            }
        }


    }

}