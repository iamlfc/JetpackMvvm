package me.hgj.jetpackmvvm.demo.app.base

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import androidx.viewbinding.ViewBinding
import com.tamsiree.rxkit.RxActivityTool
import com.tamsiree.rxkit.TLog
import me.hgj.jetpackmvvm.R
import me.hgj.jetpackmvvm.base.activity.BaseA
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.demo.app.event.AppViewModel
import me.hgj.jetpackmvvm.demo.app.util.network.NetChangeObserver
import me.hgj.jetpackmvvm.demo.app.util.network.NetStateReceiver
import me.hgj.jetpackmvvm.demo.app.util.network.NetType
import me.hgj.jetpackmvvm.demo.ui.activity.login.LoginA
import me.hgj.jetpackmvvm.event.UIEvent
import me.hgj.jetpackmvvm.ext.getAppViewModel
import me.hgj.jetpackmvvm.widget.alerterTop.Alerter
import org.greenrobot.eventbus.EventBus

/**
 * 时间　: 2019/12/21
 * 作者　: hegaojian
 * 描述　: 你项目中的Activity基类，在这里实现显示弹窗，吐司，还有加入自己的需求操作 ，如果不想用Databind，请继承
 * BaseVmActivity例如
 * abstract class BaseActivity<VM : BaseViewModel> : BaseVmActivity<VM>() {
 */
abstract class BaseAct<VM : BaseViewModel, VB : ViewBinding> : BaseA<VM, VB>() {

    //Application全局的ViewModel，里面存放了一些账户信息，基本配置信息等
    val appViewModel: AppViewModel by lazy { getAppViewModel<AppViewModel>() }
    abstract override fun initView(savedInstanceState: Bundle?)


    /**
     * 创建liveData观察者
     */
    override fun createObserver() {}


    override fun initNetWork() {

        //开启广播去监听 网络 改变事件
        NetStateReceiver.registerObserver(object : NetChangeObserver {
            override fun onNetConnected(type: NetType?) {
                if (type != null) {
//                    onNetworkConnected(type)
                    EventBus.getDefault()
                        .post(
                            UIEvent.NetWorkChange(1, type.name.toString())
                        )

                }
            }

            override fun onNetDisConnect() {
                EventBus.getDefault()
                    .post(UIEvent.NetWorkChange(0))

            }
        })

    }

    open fun ShowTopAlaret() {
        RxActivityTool.currentActivity()?.let {
            Alerter.create(it).setTitle("提示")
                .setText("当前网络异常，请您检查网络设置").setIcon(R.drawable.alerter_ic_notifications_svg)
                .setBackgroundColorRes(R.color.prompt_warning).setDuration(5 * 1000)
                .enableSwipeToDismiss().setOnClickListener {
                    val intent = Intent(Settings.ACTION_WIFI_SETTINGS)
                    startActivity(intent)
                    Alerter.hide()
                }.setOnHideListener {
                    //    TODO("这个地方需要注意,enableSwipeToDismiss 不触发这回调！ ")
                    TLog.d("弹框 藏起来了")
                    EventBus.getDefault()
                        .post(UIEvent.NetWorkChange(2))

                }?.show()
        }
    }


    /**
     *  判断网络状态
     *
     */
    private fun checkNetwork(isConnected: Boolean) {
        try {
            if (enableNetworkTip()) {
                if (isConnected) {

                    var hasNetwork = PreferencesUtils.getBoolean(this, Params.HAS_NETWORK_KEY)

                    if (hasNetwork != isConnected)
                        doReConnected()
                    if (!Alerter.isShowing()) {
                        Alerter.hide()
                    }
                } else {
                    ShowTopAlaret()
                }
            } else {
                var currentCls = RxActivityTool.currentActivity()
                when (currentCls) {
//                     首页 欢迎页  广告业 不显示这个弹框
                    LoginA::class.java -> {
                        EventBus.getDefault()
                            .postSticky(UIEvent.NetWorkChange(1))
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (!Alerter.isShowing()) {
                Alerter.hide()
            }
        } finally {
            PreferencesUtils.putBoolean(baseContext, Params.HAS_NETWORK_KEY, isConnected)


        }


    }

}