package com.yunyun.taptop.share

/**
 * Created by LFC
on 2020/5/13.
 */
object SP_Params {

    const val PREFERENCE_NAME = "CommonSP"

    //    字数限制
//     sp  中的主键
//    定位信息
    const val Time_Dis = "timedis" // 时间差

    const val Time_Service = "Time_Service" // 服务端时间

    const val MsgCounts = "Time_MsgCounts" //  获取未读消息的时间


    const val CityAllData = "CityAllData" //所有城市

    const val Location_Lat = "Location_Lat" //定位城市维度

    const val Location_Lng = "Location_Lng" //定位城市 精度

    const val Location_P = "Location_P" //定位  省

    const val Location_C = "Location_C" //定位 市

    const val Location_D = "Location_D" //定位  区

    const val Location_Ads = "Location_Ads" //定位  区

    const val Location_Code = "Location_Code" //定位城市code

    //用户选择城市信息
    const val APP_Lat = "APP_Lat" // 用户选择的 城市维度

    const val APP_Lng = "APP_Lng" // 用户选择的 城市 精度

    const val APP_P = "APP_P" //用户选择的  省

    const val APP_C = "APP_C" //用户选择的 市

    const val APP_D = "APP_D" //用户选择的  区

    const val APP_Ads = "APP_Ads" //用户选择的  区

    const val APP_Code = "APP_Code" // 用户选择的  市code

    const val APP_CheckFlag = "APP_CheckFlag" // 魔术戏法之 1显示 其他 不显示

    const val APP_ISTipWhite = "APP_ISTipWhite" //  自启动判断

    const val APP_IsFloatDialog = "APP_IsFloatDialog" //   是否开启悬浮窗


    //app 使用
    const val SystemData = "SystemData" // 系统参数 实体类

    const val AdData = "AdData" // 广告 实体类

    const val AdDataIndex = "AdDataIndex" // 广告 实体类  页码

    const val PA_HD_Logo = "PA_HD_Logo" //   服务器 App logo

    const val SearchHis = "SearchHis" //    搜索历史


    const val OSSConfigData = "OSSConfigData" //  oss 配置

    const val OSSOutTime = "OSSOutTime" //    超时时间


    const val TaskOfflineList = "TaskOfflineList" //    线下任务 类型列表

    const val TaskOnlineList = "TaskOnlineList" //    线上任务 类型列表


    //以下为 退出登录  需要清除的 内容
    const val UserInfo = "UserInfo" // 登录用户信息

    const val UserPW = "UserPW" // 用户 登录密码

    const val UserTel = "UserTel" // 用户电话

    const val Token = "Token" // 用户 token

    const val DeviceToken = "DeviceToken" // 友盟 devicetoken

    const val ClientNum = "ClientNum" // 语音账号

    const val VersionData = "VersionData" // 版本信息

    const val VoiceToken = "VoiceToken" // 语音token

    const val ASKSign = "ASKSign" // 验签标注


    const val UserID = "UserID" // 用户id

    const val UserISJPush = "UserISJPush" //  是否接受推送  -1不接收 1 接受

    const val ISRelease = "ISRelease" // 是否 是正式服务器

    const val ShareImg = "ShareImg" // 是否 是正式服务器
    const val ShareTitle = "ShareTitle" // 是否 是正式服务器
    const val ShareUrl = "ShareUrl" // 是否 是正式服务器
    const val ShareInfo = "ShareInfo" // 是否 是正式服务器
    const val FirstOpen = "FirstOpen";// 是否  安装完成后初次打开APP


}