package me.hgj.jetpackmvvm.demo.app.share;


/**
 * Created by Administrator on 2017/1/10.
 */

public class HttpIp {

    public static final String BASE_Header = "/user/v200?";

    public final static String UserNoteURL = "https://www.zxhyuser.cn/zxhy/operation_instruction/html/helper.html";  // 使用说明
    public final static String AboutUSURL = "https://www.zxhyuser.cn/zxhy/htlink/AboutUs.html";  //  关于我们
    public final static String RegisterURL = "https://www.zxhyuser.cn/zxhy/htlink/ServiceAgreement.html";  // 注册协议
//    http://tap.top/api/register
    public final static String BaseIp = "https://api.tap.top/";  // 测试服务器 域名  图片前缀
//    public final static String BaseIp = "http://api_tap.tap.top/";  // 测试服务器 域名  图片前缀
    //    public final static String BaseIp = "http://app.quxiang.fun";  // 正式服务器 域名  图片前缀
    public static final String BASE_URL = BaseIp + "/";//  正式
    public static final String BASE_APIURL = BaseIp+"api/" ;//  接口

    public static final String BASE_SALT = "hEi9qBDaMHtxOgY7";//签名密钥


    public static String BaseDataIp = BaseIp + "user/v200"; //  接口ip


    public static int ResultCodeReLogin = 401; // 登录失效
    public static int ResultCodeOK = 200; //成功
    public static int ResultCodeError = 99; // 失效


    public static int WEB_Type_Agreement = 1001; // '用户协议',
    public static int WEB_Type_Privacy = 1002; // '隐私政策',
    public static int WEB_Type_AboutUs = 1003; // '关于我们',
    public static int WEB_Type_His = 1004; // '我们的历史',
    public static int WEB_Type_Introduction = 1005; // '分公司介绍',
    public static final String BASE_SiteKey = "readtoreadproject";//通信siteKey
    public static final String BASE_Appsecret = "readtoread!@#$vfr43edc1234";//appsecret密钥值
    /**
     * 验证码类型
     */
    public static String YZMActionReg = "reg"; // '注册',
    public static String YZMActionPw = "set_pwd"; // '设置密码',
    public static String YZMActionPayPW = "set_pay_pwd"; // '设置支付密码',

    public static String H_Deviceid = "xx-deviceid"; // '设备id',
    public static String H_Salt = "xx-salt"; // '固定的签名密钥',
    public static String H_TimeStamp = "xx-timestamp"; // '时间戳',
    public static String H_Version = "xx-version"; // '版本号',
    public static String H_Randomstr = "xx-randomstr"; // '随机数',
    public static String H_Device = "xx-device"; // '设备类别 android  ios',
    public static String H_Token = "xx-token"; // '用户touken ',
    public static String H_Signature = "xx-signature"; // 签名


    /**
     * OSS
     */
    public final static String STR_BucketName = "wt-small"; //正式服务器 -test 去除
    public final static String STR_OSSImgPath = "OSSOutTime"; // path 路径
    public final static String STR_EndPoint = "http://oss-cn-qingdao.aliyuncs.com";
    public final static String STR_ImgHead = "https://" + STR_BucketName + ".oss-cn-qingdao.aliyuncs.com";


}

