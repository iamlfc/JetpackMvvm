package com.yunyun.taptop.share

/**
 * Created by LFC
on 2020/5/13.
 */
object Params {

    const val BUGLY_ID = "76e2b2867d"

    const val BASE_URL = "https://www.wanandroid.com/"

    const val LOGIN_KEY = "login"
    const val USERNAME_KEY = "username"
    const val PASSWORD_KEY = "password"
    const val TOKEN_KEY = "token"
    const val HAS_NETWORK_KEY = "has_network"

    const val TODO_NO = "todo_no"
    const val TODO_ADD = "todo_add"
    const val TODO_DONE = "todo_done"
}