package me.hgj.jetpackmvvm.demo.app.util.network;

public enum NetType {
    WIFI, CMNET, CMWAP, NONE
}