package me.hgj.jetpackmvvm.demo.app.share

import android.Manifest
import android.os.Environment
import java.io.File

/**
 * Created by LFC
 * on 2017/11/7.
 */
/**
 * 常量
 */
object Const {
    var ISDebug = false //是否是调试模式
    const val YZM_TIME: Long = 60 //验证码倒计时
    const val PW_Min_Length = 6
    const val PW_Max_Length = 20
    const val IMG_Width = 720 //宽
    const val IMG_Height = 1280 //高
    const val PageSizeDefault = 20 //默认分页数量
    const val TimeBase = 621355968000000000L //时间计算

    //    传递intent   关键字
    const val WEBTYPE = "WebType" // 跳转 web
    const val WEBTYPE_ID = "WEBID" // 跳转 web
    const val TESTIMG_URL =
        "https://img2.baidu.com/it/u=2287392023,2654300179&fm=26&fmt=auto&gp=0.jpg" //

    //    public static final String OfficeUrl = "https://docs.google.com/gview?embedded=true&url=";   //
    const val OfficeUrl = "https://view.officeapps.live.com/op/view.aspx?src=" //
    const val VideoUrl =
        "http://angliss.weiruanmeng.com/upload/default/20190819/b4a041bb0e887b4d766427464974ef80.mp4" //
    val Data_FileDirPath =
        Environment.getExternalStorageDirectory().absolutePath + File.separator + "TapTop"

    //    public static final String Data_FileDirPath = Environment.getDataDirectory().getParentFile().getAbsolutePath() + File.separator + "DownloadApi";
    val Data_FilePath = Data_FileDirPath
    val Data_Cache = Data_FileDirPath + File.separator + "ImgCache"
    val Data_Download = Data_FileDirPath + File.separator + "ImgDownLoad"
    val Data_Vcard = Data_FileDirPath + File.separator + "vcard"
    const val Data_ShareQRIMG = "myqr_img"
    const val Data_PhotoIMG = "photo.jpg"
    const val Data_LogoIMG = "logo"

    //    public static final BaseAnimatorSet DialogIn = new BounceEnter();
    //    public static final BaseAnimatorSet DialogOut = new SlideBottomExit();
    const val HttpHeadSalt = "S9gXqLL41gHj6Obg1AoZx826miavZQPL" //
    const val IMGTHUM = "_thumb320" //
    const val LogoFileNameTip = "logophoto" //
    const val LogoFileName = LogoFileNameTip + ".png" //
    const val APPPackName = "com.mdchina.thelifeorder" //
    const val APPID = "wxee33b4b38d3ff1db" //  AppID：wxee33b4b38d3ff1db  微信ID  2019121869959742
    const val ALIYUN_AUTHLOGIN_KEY =
        "n0Gn0vmhKKzYVXnBas5KcZn8QFlMVyCL/8sHmm17Olcw3D7wsl6z214kSvnmF88Jwmob5sbRUER/xDuh3sY/u9yR/VjmswMPLN4BwMuNuQcsnkfafsDtTI0z8d0OWOpR6NU24kW9H6bgNWJ/txYcOVgcvZk7VbtVaHANWOmxgaXDlJOuN3i3C16kE4EW/TpKLDKbmRJwSmAg8BNwqRfivmXrMUbU1LU1T34uN1Pi3Aa77OJE2pydm6EiPjnGwY3FuiZORki/2l78oEe6VIRUaIkxLigKqFfjPC2tbIt7RFpWy/pC7ih+cw=="


    const val SP_LANGUAGE = "SP_LANGUAGE"
    const val SP_COUNTRY = "SP_COUNTRY"
    const val ARG_PARAM1 = "param1"
    const val ARG_PARAM2 = "param2"
    const val ARG_PARAM3 = "param3"
    const val ARG_PARAM4 = "param4"
    const val ComentPageSize = "3" //详情中 评论的数量
    const val FGHomeTag = "FGHomeTag" // 首页tag
    const val FGPublishTag = "FGPublishTag" // 发布tag
    const val FGMsgTag = "FGMsgTag" // 消息tag
    const val FGMineTag = "FGMineTag" // 我的tag
    const val LatDefault = 22.52858 // 默认维度
    const val LngDefault = 113.951397 // 我的精度

    /**
     * 需要进行检测的权限数组
     */

    // 读写
    val STORAGE_PERMISSIONS = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    // 相机访问
    val CAMERA_PERMISSIONS = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )

    // 通讯录
    val CONTACTS_PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_CONTACTS,
        Manifest.permission.GET_ACCOUNTS,
        Manifest.permission.READ_CONTACTS
    )

    // 拨号
    val PHONE_CALL_PERMISSIONS = arrayOf(
        Manifest.permission.READ_CALL_LOG,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.CALL_PHONE,
        Manifest.permission.WRITE_CALL_LOG,
        Manifest.permission.USE_SIP,
        Manifest.permission.PROCESS_OUTGOING_CALLS,
        Manifest.permission.ADD_VOICEMAIL
    )

    // 日历
    val CALENDAR_PERMISSIONS = arrayOf(
        Manifest.permission.READ_CALENDAR,
        Manifest.permission.WRITE_CALENDAR
    )

    // 录音
    val RECORD_PERMISSIONS = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.RECORD_AUDIO
    )

    // 短信收发
    val SMS_PERMISSIONS = arrayOf(
        Manifest.permission.READ_SMS,
        Manifest.permission.RECEIVE_WAP_PUSH,
        Manifest.permission.RECEIVE_MMS,
        Manifest.permission.RECEIVE_SMS,
        Manifest.permission.SEND_SMS
    )

    // 位置
    val GPS_PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_WIFI_STATE
    )
}