package me.hgj.jetpackmvvm.demo.ui.activity.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import me.hgj.jetpackmvvm.base.activity.BaseA
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.demo.app.base.BaseAct
import me.hgj.jetpackmvvm.demo.databinding.ActivityLoginBinding
import me.hgj.jetpackmvvm.demo.databinding.ActivityTestMyBinding


class LoginA : BaseAct<BaseViewModel, ActivityLoginBinding>() {
    override fun initView(savedInstanceState: Bundle?) {

    }

    override val bindingInflater: (LayoutInflater) -> ActivityLoginBinding
        get() = ActivityLoginBinding::inflate

}