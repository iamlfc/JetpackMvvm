package me.hgj.jetpackmvvm.event

/**
 *  事件总线相关
 *  @author lfc-LFC
 *  created at 2021/6/29 17:31
 */

interface UIEvent {
    class SyncServerTime {
    }

    /**
     * 网络变化监听
     */
    class NetWorkChange constructor(type: Int, strInfo: String = "") {

    }

}