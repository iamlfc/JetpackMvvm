package me.hgj.jetpackmvvm.base

import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import dagger.hilt.android.scopes.ActivityScoped
import me.hgj.jetpackmvvm.R
import me.hgj.jetpackmvvm.util.ToastUtils
import me.hgj.jetpackmvvm.widget.CustomProgressDialog
import javax.annotation.Signed
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @Description:  UI 统一调配
 * @Author:  lfc
 * @Email:    iamlifuchang@163.com
 * @CreateTime:     2021/6/23 9:43
 * @UpdateRemark:   UI 统一调配
 */
class UIController(var ctx: Context) {
    var loadingDialog: CustomProgressDialog? = null

    init {
        loadingDialog = CustomProgressDialog(ctx)
    }

    fun showLoading(message: String = "请求网络中...", cancelEnable: Boolean = true) {
        loadingDialog?.apply {
            this.setCancelable(cancelEnable)
            loadingDialog?.setMessage(message)
            loadingDialog?.show()
        }

    }

    fun dismissLoading() {
        loadingDialog?.dismiss()
    }

    fun showConfirmDialog(ctx: Context, message: String?, cancelEnable: Boolean = true) {

        if (message.isNullOrEmpty()) {
            return
        }
        AlertDialog.Builder(ctx).setTitle(ctx.getString(R.string.default_dialog_title))
            .setMessage(message)
            .setCancelable(cancelEnable)
            .setPositiveButton(
                ctx.getString(R.string.confirm)
            ) { dialog, _ -> dialog.dismiss() }
            .create().show()
    }

    fun showToast(ctxs: Context = ctx, strMsg: String?) {
        if (strMsg.isNullOrBlank()) return
        ToastUtils.showToask(ctx, strMsg)

    }
}