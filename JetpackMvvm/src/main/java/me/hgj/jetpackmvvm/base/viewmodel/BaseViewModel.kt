package me.hgj.jetpackmvvm.base.viewmodel

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.hgj.jetpackmvvm.base.ViewBehavior
import me.hgj.jetpackmvvm.event.BaseStateEvent
import me.hgj.jetpackmvvm.event.EventArgs

/**
 * 作者　: hegaojian
 * 时间　: 2019/12/12
 * 描述　: ViewModel的基类 使用ViewModel类，放弃AndroidViewModel，原因：用处不大 完全有其他方式获取Application上下文
 */
open class BaseViewModel : ViewModel(), ViewBehavior, LifecycleObserver {

    /**
     * 分页加载页数
     */
    var pageNum = 1

    /**
     * 可基于此类进行扩展
     */
    private val pageStateEvent = MutableLiveData<BaseStateEvent>()
    open fun getStateInfo(): LiveData<BaseStateEvent> {
        return pageStateEvent
    }


    override fun showLoading(msg: String, cancelEnable: Boolean) {
        pageStateEvent.value = BaseStateEvent(
            event = EventArgs.SHOW_LOADING,
            message = msg,
            cancelEnable = cancelEnable
        )
    }

    override fun showConfirmDialog(content: String) {
        pageStateEvent.value = BaseStateEvent(
            event = EventArgs.SHOW_CONFIRM,
            content = content,
        )
    }

    override fun hideAllDialog() {
        pageStateEvent.value = BaseStateEvent(event = EventArgs.HIDE_DIALOG)
    }

    override fun showError(code: Int, message: String?) {
        pageStateEvent.value = BaseStateEvent(
            event = EventArgs.SHOW_ERROR,
            code = code,
            errorMsg = message
        )
    }

    override fun showToast(message: String) {
        pageStateEvent.value = BaseStateEvent(
            event = EventArgs.SHOW_TOAST,
            toastMsg = message
        )
    }


}