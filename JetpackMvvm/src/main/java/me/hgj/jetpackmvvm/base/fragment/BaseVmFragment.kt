package me.hgj.jetpackmvvm.base.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import me.hgj.jetpackmvvm.base.UIController
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.event.EventArgs
import me.hgj.jetpackmvvm.ext.getVmClazz
import me.hgj.jetpackmvvm.network.manager.NetState
import me.hgj.jetpackmvvm.network.manager.NetworkStateManager

/**
 * 作者　: hegaojian
 * 时间　: 2019/12/12
 * 描述　: ViewModelFragment基类，自动把ViewModel注入Fragment
 */

abstract class BaseVmFragment<VM : BaseViewModel, VB : ViewBinding> : Fragment() {
    lateinit var uiShow: UIController

    private val handler = Handler()

    //是否第一次加载
    private var isFirst: Boolean = true

    /**
     * ViewModel
     */
    abstract val modelClass: Class<VM>?

    @Nullable
    var mViewModel: VM? = null

    /**
     * ViewBinding
     */
    private var _binding: ViewBinding? = null
    abstract val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> VB

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    lateinit var mActivity: AppCompatActivity

    /**
     * 当前Fragment绑定的视图布局
     */
    abstract fun layoutId(): Int


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = bindingInflater.invoke(inflater, container, false)
        modelClass?.let {
            mViewModel = ViewModelProvider(this).get(it)
        }
        return requireNotNull(_binding).root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
        uiShow = UIController(context)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isFirst = true
        mViewModel = createViewModel()
        initView(savedInstanceState)
        createObserver()
        initBaseActionEvent()
        initData()
    }

    /**
     * 网络变化监听 子类重写
     */
    open fun onNetworkStateChanged(netState: NetState) {}

    /**
     * 创建viewModel
     */
    private fun createViewModel(): VM {
        return ViewModelProvider(this).get(getVmClazz(this))
    }

    /**
     * 初始化view
     */
    abstract fun initView(savedInstanceState: Bundle?)

    /**
     * 懒加载
     */
    abstract fun lazyLoadData()

    /**
     * 创建观察者
     */
    abstract fun createObserver()

    override fun onResume() {
        super.onResume()
        onVisible()
    }

    /**
     * 是否需要懒加载
     */
    private fun onVisible() {
        if (lifecycle.currentState == Lifecycle.State.STARTED && isFirst) {
            // 延迟加载 防止 切换动画还没执行完毕时数据就已经加载好了，这时页面会有渲染卡顿
            handler.postDelayed({
                lazyLoadData()
                //在Fragment中，只有懒加载过了才能开启网络变化监听
                NetworkStateManager.instance.mNetworkStateCallback.observeInFragment(
                    this,
                    Observer {
                        //不是首次订阅时调用方法，防止数据第一次监听错误
                        if (!isFirst) {
                            onNetworkStateChanged(it)
                        }
                    })
                isFirst = false
            }, lazyLoadTime())
        }
    }

    /**
     * Fragment执行onCreate后触发的方法
     */
    open fun initData() {}

    /**
     * 全局配置
     */
    @SuppressLint("FragmentLiveDataObserve")
    private fun initBaseActionEvent() {
        mViewModel?.getStateInfo()?.observe(this, Observer {
            when (it.event) {
                EventArgs.SHOW_LOADING -> showLoading(it.message, it.cancelEnable)
                EventArgs.DO_NOTHING, EventArgs.HIDE_DIALOG -> hideLoading()
                EventArgs.SHOW_ERROR -> {
                    showToast(it.errorMsg)
                }
                EventArgs.SHOW_CONFIRM -> {
                    hideLoading()
                    showConfirmDialog(it.content, true)
                }
                EventArgs.SHOW_TOAST -> {
                    if (it.toastMsg.isNotEmpty()) {
                        showToast(it.toastMsg)
                    } else {
                        showToast(it.message)
                    }
                }
                else -> {

                }
            }
        })
    }
    //<editor-fold desc="-ui控制">

    fun showConfirmDialog(message: String?, cancelEnable: Boolean) {
        if (this.isDetached)
            return
        uiShow?.showConfirmDialog(this, message, cancelEnable)
    }

    fun showToast(message: String?) {
        if (this.isDetached)
            return
        uiShow?.showToast(this, message)
    }

    fun showLoading(message: String? = "", cancelEnable: Boolean) {
        if (this.isDetached)
            return
        uiShow?.showLoading(message.toString(), cancelEnable)
    }

    fun hideLoading() {
        uiShow?.dismissLoading()
    }

    //</editor-fold>

    /**
     * 将非该Fragment绑定的ViewModel添加 loading回调 防止出现请求时不显示 loading 弹窗bug
     * @param viewModels Array<out BaseViewModel>
     */
    protected fun addLoadingObserve(vararg viewModels: BaseViewModel) {
        /*    viewModels.forEach { viewModel ->
                //显示弹窗
                viewModel.loadingChange.showDialog.observeInFragment(this, Observer {
                    showLoading(it)
                })
                //关闭弹窗
                viewModel.loadingChange.dismissDialog.observeInFragment(this, Observer {
                    dismissLoading()
                })
            }*/
    }

    /**
     * 延迟加载 防止 切换动画还没执行完毕时数据就已经加载好了，这时页面会有渲染卡顿  bug
     * 这里传入你想要延迟的时间，延迟时间可以设置比转场动画时间长一点 单位： 毫秒
     * 不传默认 300毫秒
     * @return Long
     */
    open fun lazyLoadTime(): Long {
        return 300
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(null)
    }
}