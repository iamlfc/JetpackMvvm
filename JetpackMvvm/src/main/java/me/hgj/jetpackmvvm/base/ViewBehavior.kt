package me.hgj.jetpackmvvm.base

import androidx.annotation.Nullable

/**
 * @Description:
 * @Author:   Hsp
 * @Email:    1101121039@qq.com
 * @CreateTime:     2020/8/25 14:24
 * @UpdateRemark:   更新说明：
 */
interface ViewBehavior {


    /**
     * 显示加载，cancelEnable 进度框是否可取消
     */
    fun showLoading(msg: String = "加载中...", cancelEnable: Boolean = true)

    /**
     * 显示确认弹框
     */
    fun showConfirmDialog(content: String)

    /**
     * 隱藏
     */
    fun hideAllDialog()

    /**
     * 显示空頁面
     */
    fun showError(code: Int, message: String?)

    /**
     * 显示Toast提示内容
     */
    fun showToast(message: String)


}