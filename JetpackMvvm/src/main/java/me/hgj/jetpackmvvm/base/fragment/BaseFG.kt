package me.hgj.jetpackmvvm.base.fragment

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import me.hgj.jetpackmvvm.base.UIController
import me.hgj.jetpackmvvm.base.viewmodel.BaseViewModel
import me.hgj.jetpackmvvm.event.EventArgs
import me.hgj.jetpackmvvm.ext.getVmClazz
import me.hgj.jetpackmvvm.network.manager.NetState
import me.hgj.jetpackmvvm.network.manager.NetworkStateManager

/**
 * 作者　: hegaojian
 * 时间　: 2019/12/12
 * 描述　: ViewModelFragment基类，自动把ViewModel注入Fragment
 */

abstract class BaseFG<VM : BaseViewModel, VB : ViewBinding> : Fragment() {
    /**
     * 上下文context
     */
    var baseContext: Activity = Activity()
    private val handler = Handler()

    //是否第一次加载
    private var isFirst: Boolean = true

    lateinit var mViewModel: VM

    lateinit var mActivity: AppCompatActivity
    lateinit var uiShow: UIController

    /**
     * ViewBinding
     */
    private var _binding: ViewBinding? = null
    abstract val bindingInflater: (LayoutInflater) -> VB

    @Suppress("UNCHECKED_CAST")
    protected val binding: VB
        get() = _binding as VB

    /**
     * 当前Fragment绑定的视图布局
     */
    abstract fun layoutId(): Int


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mActivity = context as AppCompatActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        baseContext = requireActivity()
        isFirst = true
        mViewModel = createViewModel()
        initView(savedInstanceState)
        createObserver()
        registorDefUIChange()
        initData()
    }

    /**
     * 网络变化监听 子类重写
     */
    open fun onNetworkStateChanged(netState: NetState) {}

    /**
     * 创建viewModel
     */
    private fun createViewModel(): VM {
        return ViewModelProvider(this).get(getVmClazz(this))
    }

    /**
     * 初始化view
     */
    abstract fun initView(savedInstanceState: Bundle?)

    /**
     * 懒加载
     */
    abstract fun lazyLoadData()

    /**
     * 创建观察者
     */
    abstract fun createObserver()

    override fun onResume() {
        super.onResume()
        onVisible()
    }

    /**
     * 是否需要懒加载
     */
    private fun onVisible() {
        if (lifecycle.currentState == Lifecycle.State.STARTED && isFirst) {
            // 延迟加载 防止 切换动画还没执行完毕时数据就已经加载好了，这时页面会有渲染卡顿
            handler.postDelayed({
                lazyLoadData()
                //在Fragment中，只有懒加载过了才能开启网络变化监听
                NetworkStateManager.instance.mNetworkStateCallback.observeInFragment(
                    this,
                    Observer {
                        //不是首次订阅时调用方法，防止数据第一次监听错误
                        if (!isFirst) {
                            onNetworkStateChanged(it)
                        }
                    })
                isFirst = false
            }, lazyLoadTime())
        }
    }

    /**
     * Fragment执行onCreate后触发的方法
     */
    open fun initData() {}


    /**
     * 注册 UI 事件
     */
    private fun registorDefUIChange() {
        mViewModel.getStateInfo().observe(this, Observer {
            when (it.event) {
                EventArgs.SHOW_LOADING -> {
                    showLoading(it.message, it.cancelEnable)
                }
                EventArgs.DO_NOTHING, EventArgs.HIDE_DIALOG -> hideLoading()
                EventArgs.SHOW_ERROR -> {
                    hideLoading()
                    showToast(it.errorMsg)
                }
                EventArgs.SHOW_CONFIRM -> {
                    showConfirmDialog(it.content, true)
                }
                EventArgs.SHOW_TOAST -> {

                    if (it.toastMsg.isNotEmpty()) {
                        showToast(it.toastMsg)
                    } else {
                        showToast(it.message)
                    }
                }
                else -> {

                }
            }
        })
    }

    /**
     * 将非该Fragment绑定的ViewModel添加 loading回调 防止出现请求时不显示 loading 弹窗bug
     * @param viewModels Array<out BaseViewModel>
     */
    protected fun addLoadingObserve(vararg viewModels: BaseViewModel) {
        /*  viewModels.forEach { viewModel ->
              //显示弹窗
              viewModel.loadingChange.showDialog.observeInFragment(this, Observer {
                  showLoading(it)
              })
              //关闭弹窗
              viewModel.loadingChange.dismissDialog.observeInFragment(this, Observer {
                  dismissLoading()
              })
          }*/
    }

    /**
     * 延迟加载 防止 切换动画还没执行完毕时数据就已经加载好了，这时页面会有渲染卡顿  bug
     * 这里传入你想要延迟的时间，延迟时间可以设置比转场动画时间长一点 单位： 毫秒
     * 不传默认 300毫秒
     * @return Long
     */
    open fun lazyLoadTime(): Long {
        return 300
    }

    //<editor-fold desc="-ui控制">

    fun showConfirmDialog(message: String?, cancelEnable: Boolean) {
        if (this.isHidden)
            return
        uiShow?.showConfirmDialog(baseContext, message, cancelEnable)
    }

    fun showToast(message: String?) {
        if (this.isHidden)
            return
        uiShow?.showToast(baseContext, message)
    }

    fun showLoading(message: String? = "", cancelEnable: Boolean=false) {
        if (this.isHidden)
            return
        uiShow?.showLoading(message.toString(), cancelEnable)
    }

    fun hideLoading() {
        uiShow?.dismissLoading()
    }

    //</editor-fold>
}