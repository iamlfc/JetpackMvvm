package me.hgj.jetpackmvvm.util

import android.app.ActivityManager
import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import android.widget.Toast

/**
 * Created by LFC
on 2020/4/30.
 */
class ToastUtils {
    companion object {

        private var mToast: Toast? = null
        private var ImgToast: Toast? = null
        fun showToask(context: Context?, text: String) {
            if (context == null) return
            if (text.contains("，认证失败，无法访问系统资源")) {
                return
            }
            if (TextUtils.isEmpty(text) || text == "暂无数据") return
            if (mToast == null) {
                mToast = Toast.makeText(context, null, Toast.LENGTH_SHORT)
                mToast?.setGravity(Gravity.CENTER, 0, 0);
                mToast?.setText(text)
            } else {
                try {
                    mToast?.setText(text)
                } catch (e: Exception) {
                    mToast = null
                    mToast =
                        Toast.makeText(context, null, Toast.LENGTH_SHORT)
                    mToast?.setGravity(Gravity.CENTER, 0, 0);

                    mToast?.setText(text)
                }
                //            mToast.setDuration(duration);
            }
            //如果在后台 则不显示 dialog
            if (!isBackground(context)) {
                mToast?.show()
            }
            //        Toast.makeText(context, tip, Toast.LENGTH_SHORT).show();
        }

        fun showLongToask(context: Context?, text: String) {
            if (context == null) return
            if (TextUtils.isEmpty(text) || text == "暂无数据") return
            if (mToast == null) {
                mToast = Toast.makeText(
                    context,
                    null,
                    Toast.LENGTH_LONG
                )
                mToast?.setGravity(Gravity.CENTER, 0, 0);

                mToast?.setText(text)
            } else {
                try {
                    mToast?.setText(text)
                } catch (e: Exception) {
                    mToast = null
                    mToast =
                        Toast.makeText(
                            context,
                            null,
                            Toast.LENGTH_LONG
                        )
                    mToast?.setGravity(Gravity.CENTER, 0, 0);

                    mToast?.setText(text)
                }
                //            mToast.setDuration(duration);
            }
            //如果在后台 则不显示 dialog
            if (!isBackground(context)) {
                mToast?.show()
            }
            //        Toast.makeText(context, tip, Toast.LENGTH_SHORT).show();
        }

        fun showToask(
            context: Context?,
            text: String?,
            duration: Int
        ) {
            if (mToast == null) {
                mToast = Toast.makeText(context, null, duration)
                mToast?.setGravity(Gravity.CENTER, 0, 0);

                mToast?.setText(text)
            } else {
                mToast?.setText(text)
                mToast?.setDuration(duration)
            }
            mToast?.show()
        }

        fun showExitToask(context: Context?, text: String) {
            context?.apply {
                if (text == "暂无数据") return
                val toast = Toast.makeText(context, null, Toast.LENGTH_SHORT)
                toast.setText(text)
                //如果在后台 则不显示 dialog
                if (!isBackground(context)) {
                    toast.show()
                }
            }

        }

        fun hideToask() {
            if (mToast == null) {
                return
            } else {
                try {
                    mToast?.cancel()
                    //                mToast = null;
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.e("loan", "Exception  $e")
                }
            }
        }

        fun isBackground(context: Context): Boolean {
            val activityManager = context
                .getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val appProcesses = activityManager
                .runningAppProcesses
            for (appProcess in appProcesses) {
                if (appProcess.processName == context.packageName) { /*
                BACKGROUND=400 EMPTY=500 FOREGROUND=100
                GONE=1000 PERCEPTIBLE=130 SERVICE=300 ISIBLE=200
                 */
                    Log.i(
                        context.packageName, "此appimportace ="
                                + appProcess.importance
                                + ",context.getClass().getName()="
                                + context.javaClass.name
                    )
                    return if (appProcess.importance != ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        Log.i(
                            context.packageName, "处于后台"
                                    + appProcess.processName
                        )
                        true
                    } else {
                        Log.i(
                            context.packageName, "处于前台"
                                    + appProcess.processName
                        )
                        false
                    }
                }
            }
            return false
        }


    }

}